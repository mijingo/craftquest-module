<?php
/**
 * craftquest module for Craft CMS 3.x
 *
 * General CQ business.
 *
 * @link      https://craftquest.io
 * @copyright Copyright (c) 2019 Ryan Irelan
 */

namespace modules\craftquestmodule\services;

use modules\craftquestmodule\CraftquestModule;

use Craft;
use craft\base\Component;

/**
 * CraftquestModuleService Service
 *
 * All of your module’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other modules can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    Ryan Irelan
 * @package   CraftquestModule
 * @since     1.0.0
 */
class CraftquestModuleService extends Component
{
    // Public Methods
    // =========================================================================

    /**
     * This function can literally be anything you want, and you can have as many service
     * functions as you want
     *
     * From any other plugin/module file, call it like this:
     *
     *     CraftquestModule::$instance->craftquestModuleService->exampleService()
     *
     * @return mixed
     */
    public function exampleService()
    {
        $result = 'something';

        return $result;
    }
}
