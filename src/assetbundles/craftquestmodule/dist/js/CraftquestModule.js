/**
 * craftquest module for Craft CMS
 *
 * craftquest JS
 *
 * @author    Ryan Irelan
 * @copyright Copyright (c) 2019 Ryan Irelan
 * @link      https://craftquest.io
 * @package   CraftquestModule
 * @since     1.0.0
 */
