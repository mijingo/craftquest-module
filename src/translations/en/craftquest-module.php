<?php
/**
 * craftquest module for Craft CMS 3.x
 *
 * General CQ business.
 *
 * @link      https://craftquest.io
 * @copyright Copyright (c) 2019 Ryan Irelan
 */

/**
 * craftquest en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('craftquest-module', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Ryan Irelan
 * @package   CraftquestModule
 * @since     1.0.0
 */
return [
    'craftquest plugin loaded' => 'craftquest plugin loaded',
];
